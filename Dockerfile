FROM debian
MAINTAINER Hanno Wagner <wagner@rince.de>
# Pull base image
# install inn
RUN \
mkdir /etc/news 
COPY inn.conf /etc/news/inn.conf
RUN \
apt-get update -y && \
echo n | apt-get -y --no-install-recommends install inn2 inn2-inews openssl grep sed coreutils gnupg bash vim

# define default port
EXPOSE 119

# define mountable directories
VOLUME ["/var/lib/news", "/etc/news", "/var/log/news", "/var/spool/news" ] 
RUN mkdir -p /run/news && chown news:news /run/news && chmod 755 /run/news
RUN mkdir -p /var/www/inn && chown news:news /var/www/inn && chmod 755 /var/www/inn
USER  news
ENV INNFLAGS="-f"
CMD [ "/usr/lib/news/bin/rc.news" ]
# define default command

