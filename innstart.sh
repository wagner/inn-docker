docker run \
        --name=inn2-news1 \
                --hostname=luggage.rince.de \
        -v /volume1/docker/inn2/etc:/etc/news \
        -v /volume1/docker/inn2/lib:/var/lib/news \
        -v /volume1/docker/inn2/log:/var/log/news \
        -v /volume1/docker/inn2/spool:/var/spool/news \
        -p 119:119 \
        inn2:latest

